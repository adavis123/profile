<?php
	class Files{
		private $path = 'files';
		
			
		public function search(){
			$files = scandir($this->path.'/');
			
			$allfiles = $this->output($files);
			
			return $allfiles;
		
		}
		public function output($files){
			$containers = [];
			$iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($files), RecursiveIteratorIterator::SELF_FIRST);
			foreach ( $iterator as $key => $value ) {							
				if ($value == '.' || $value == '..' )	continue;
				$containers[] = '<div><div class="'.$value.'">';
				$newpath = $this->path . '/' . $value . '/';
				$dir = scandir($newpath);
				$contents = [];
				foreach ($dir as $d => $subdir) {
					if ($subdir == '.' || $subdir == '..'  || $subdir == null)	continue;
					
					$contents[]= $this->getExtension($newpath.$subdir);
					
				}
				
				$containers[]= implode($contents,'');
				$containers[] = '</div></div>';
				
			}
			//var_dump($containers);
			return implode($containers,'');
			
		}
		public function getExtension($file){
			//$ext = pathinfo($file);
			$size = pathinfo($file, PATHINFO_EXTENSION);
			//$size = getimagesize($file); 
			switch ($size) { 
				case "gif": 
					return $this->getImages($file,'gif');
					break; 
				case "jpg": 
					return $this->getImages($file,'jpg'); 
					break; 
				case "jpeg": 
					return $this->getImages($file,'jpeg'); 
					break; 
				case "png": 
					return $this->getImages($file,'png');
					break; 
				case "bmp": 
					return $this->getImages($file,'bmp'); 
					break;
				case "txt": 
					return $this->getContent($file,'txt'); 
					break; 					
			} 
			
		}
		public function getContent($file , $mime){
			$content = '<span class="'.$mime.'">';
			$content .= file_get_contents($file);
			$content .= '<span>';
			return $content;
			
		}
		public function getImages($path,$mime){
			$img = '<div class="project-image"><img src="'.$path.'" class="'.$mime.'"/></div>';
			return $img;
		}
	}

?>