<?php
  include 'class/class.modernizr.php';
  include 'class/class.os.php';
  include 'class/class.browser.php';
  include 'class/class.files.php';
?>

<?php 
	$modernizr = new Modernizr;
	$browser = new Browser;
	$os = new OS;			 
	$m = $modernizr::boo();
	$b = $browser;
	$o = $os->getOS();			
	$all['modernizr'] = (array)$m;
	$all['os'] = (array) $o;
	$all['browser'] = array_values((array)$b);
	$all['modernizr']['w']	= (array)$all['modernizr']['w'];
?>
<html>
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Dev in Paradise</title>
		<?php
			if($all['modernizr']['w']['screen-width'] < 500){
		?>
		<link href="assets/css/mobile-app.css" rel="stylesheet"/>
		<?php
			}else{
		?>
		
		<link href="assets/css/app.css" rel="stylesheet"/>
		<!--<link href="assets/css/mediaqueries.css" rel="stylesheet"/>-->
		<?php } ?>
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.gsap.min.js"></script>
		<script src="assets/js/ScrollToPlugin.min.js"></script>		
		<script src="assets/js/EasePack.min.js"></script>
		<script src="assets/js/CSSPlugin.min.js"></script>
		<script src="assets/js/TweenLite.min.js"></script>
		<script src="assets/js/TweenMax.min.js"></script>
		<script src="assets/js/swipe.js"></script>
		<script src="assets/js/touchswipe.js"></script>
		<script src="assets/js/app.js"></script>
		<?php
			if($all['modernizr']['w']['screen-width'] < 500){
		?> 
		
		<script src="assets/js/mobile-app.js"></script>
		<?php } ?>
	</head>
	
	<body>
	
		<?php
				if($all['modernizr']['w']['screen-width'] < 500){					
			?>
		<div id="mobile-menu" class="side-nav">
			<ul class="nav-ul">
					<li><a href="#about">About</a></li>
					<li><a href="#profile">Profile</a></li>
					<li><a href="#contact">Contact</a></li>				
				</ul>
		</div>
		<?php } ?>
		<nav class="top-nav">
			<?php
				if($all['modernizr']['w']['screen-width'] < 500){					
			?>	
				<a href="#mobile-menu" class="mobile-link">
					<span class="bar"></span>
					<span class="bar"></span>
					<span class="bar"></span>
				</a>
			<?php } ?>
			<span class="headers">
				<a href="#slide-1">
				<h2>Dev in Paradise</h2>
				<cite>Andrew Davis</cite>
				</a>
			</span>
			
			<?php
				if($all['modernizr']['w']['screen-width'] > 500){					
			?>
				<ul class="nav-ul">
					<li><a href="#about">About</a></li>
					<li><a href="#profile">Profile</a></li>
					<li><a href="#contact">Contact</a></li>				
				</ul>
			<?php } ?>
		</nav>

		<div class="slides-container">
			<div class="slide" id="about">
				<div class="centered">
					<h1>Fullscreen slides with GSAP</h1>
					<p>Let's go to the <span class="go-next">next slide</span>.</p>
				</div>
			</div>
			<div class="slide" id="profile">
				<div class="centered">
					<h2>My Projects</h2>
					<!--<div class="left fade"></div>-->
					<div id='mySwipe' style='max-width:100%;margin:0 auto' class='swipe'>
					  <div class='swipe-wrap'>
						
						<?php 
						$files = new Files;
						$allfiles = $files->search();
						echo $allfiles;
						?>
					  </div>
					</div>
					<!--<div class="right fade"></div>-->
					<?php
				if($all['modernizr']['w']['screen-width'] > 500){					
			?>
					<div class="nav-links" style='text-align:center;padding-top:20px;'>
  
					  <button class="prev-swipe" onclick='mySwipe.prev()'></button> 
					  <button class="next-swipe" onclick='mySwipe.next()'></button>

					</div>
					<?php
						}
					?>
				</div>
			</div>
			<div class="slide" id="contact">
				<div class="centered">
					<h1>Use mouse wheel</h1>

					<p>No, really. Try to scroll up and down with the mouse wheel to switch between slides.</p>
				</div>
			</div>
			
		</div>	
	</body>
</html>

